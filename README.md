To run the test, simply execute : mvn clean install

To run within docker container:

docker run -ti -v "$(pwd)":/usr/src/mymaven -v maven-repo:/root/.m2 -w /usr/src/mymaven maven:3-jdk-8-alpine mvn clean install