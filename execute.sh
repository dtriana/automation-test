#!/bin/sh
IP_SS=$(/usr/local/bin/VBoxManage guestproperty get $WINBOX /VirtualBox/GuestInfo/Net/2/V4/IP | sed -e "s/^Value:\ //")

/usr/local/bin/docker run -e IP_SS=$IP_SS -t --rm -v "$(pwd)":/usr/src/mymaven -v maven-repo:/root/.m2 \
  -e GO_UID=$GO_UID \
  -e GO_GID=$GO_GID \
  -e GO_PIPELINE_COUNTER=$GO_PIPELINE_COUNTER \
  -w /usr/src/mymaven maven:3-jdk-8-alpine mvn clean install 
