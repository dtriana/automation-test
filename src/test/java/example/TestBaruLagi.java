package example;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.net.URL;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestBaruLagi {

    private WebDriver driver;

    @Test
    public void myTestYangBaru() throws Exception {
        
        String ipSeleniumServer = System.getenv("IP_SS");
        String urlSeleniumServer = "http://"+ipSeleniumServer+":4444/wd/hub";
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();

        WebDriver driver = new RemoteWebDriver(
                new URL(urlSeleniumServer),
                capabilities);

        driver.get("http://www.facebook.com");

        // RemoteWebDriver does not implement the TakesScreenshot class
        // if the driver does have the Capabilities to take a screenshot
        // then Augmenter will add the TakesScreenshot methods to the instance
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot) augmentedDriver).
                getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("screenshot1.png"));
        driver.quit();
    }
}
