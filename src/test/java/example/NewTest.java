package example;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewTest {

    private WebDriver driver;

    @Test
    public void myTest() throws Exception {
        String ipSeleniumServer = System.getenv("IP_SS");
        String urlSeleniumServer = "http://"+ipSeleniumServer+":4444/wd/hub";

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
System.out.println(urlSeleniumServer);
        WebDriver driver = new RemoteWebDriver(
                new URL(urlSeleniumServer),
                capabilities);

        driver.get("http://www.google.com");
        driver.findElement(By.id("lst-ib")).sendKeys("Voila!");
        driver.findElement(By.id("lst-ib")).sendKeys(Keys.ENTER);
        
        WebElement element = (new WebDriverWait(driver, 10)).
                until(ExpectedConditions.presenceOfElementLocated(By.id("rso")));
        
        
        // RemoteWebDriver does not implement the TakesScreenshot class
        // if the driver does have the Capabilities to take a screenshot
        // then Augmenter will add the TakesScreenshot methods to the instance
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot) augmentedDriver).
                getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("screenshot.png"));
        driver.quit();
    }
}
